﻿using Enyim.Caching;
using Enyim.Caching.Configuration;
using Enyim.Caching.Memcached;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memcached
{
    class Program
    {
        static void Main(string[] args)
        {
            //feature_x branch added
            MemcachedClientConfiguration config = new MemcachedClientConfiguration();
            config.AddServer("127.0.0.1", 11211);
            config.Protocol = MemcachedProtocol.Binary;
            MemcachedClient client = new MemcachedClient(config);

            List<Person> kisiler = new List<Person>()
            {
                new Person { Name = "Mustafa", Age = 21 },
                new Person { Name = "Tayyip", Age = 54 },
                new Person { Name = "Yetiş", Age = 23 }
            };
            bool result = client.Store(StoreMode.Set, "kisiler", kisiler);
            foreach (var item in kisiler)
            {
                Console.WriteLine(item.Name , item.Age);
            }
            bool removedPerson = client.Remove("kisiler");

            List<Person> value = client.Get<List<Person>>("kisiler");
        }
    }

    [Serializable]
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
